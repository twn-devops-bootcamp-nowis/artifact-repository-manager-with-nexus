# Artifact Repository Manager with Nexus

## Overview
This project, part of the "Artifact Repository Manager with Nexus" module in the TechWorld with Nana DevOps Bootcamp, involves setting up Nexus to manage various artifacts for different projects within the company. It includes managing NodeJS and Java applications, ensuring efficient access and maintenance of these artifacts.

## Features
- Nexus server setup for artifact management
- Hosted npm and maven repositories for NodeJS and Java applications
- Automated scripts for fetching the latest versions of artifacts

## Getting Started

### Prerequisites
- Access to a server for Nexus installation
- Basic understanding of Nexus, npm, and Maven
- Familiarity with Node.js, Java, and DigitalOcean droplets

## Installation and Setup

### Exercise 1: Install Nexus on a Server
**Objective:** Learn how to install Nexus on a server for managing artifacts.
- **Choose a Server:** Select a suitable server for Nexus installation.
- **Download Nexus:** Obtain Nexus from the official website.
- **Installation:** Follow the installation instructions applicable to your server's OS.
- **Verification:** Access the Nexus UI via a web browser to confirm successful installation.

### Exercise 2: Create npm Hosted Repository
**Objective:** Create a new npm hosted repository in Nexus.
- **Access Nexus UI:** Log into Nexus and navigate to the repository section.
- **Repository Creation:** Configure the settings for the new npm hosted repository.
- **Save and Verify:** Confirm the repository is operational.

### Exercise 3: Create User for Team 1
**Objective:** Set up a Nexus user for Team 1 with specific permissions.
- **User Management:** Open the user management section in Nexus.
- **Create User:** Define a new user account with relevant details.
- **Assign Permissions:** Grant access specifically to the npm repository.
- **Save and Distribute:** Save the user settings and provide credentials to Team 1.

### Exercise 4: Build and Publish npm tar
**Objective:** Build and publish a Node.js tar package to the npm repository.
- **Prepare Node.js Project:** Navigate to a Node.js project directory.
- **Build Package:** Execute build commands to prepare the tar package.
- **Publish:** Use npm to publish the package to the Nexus repository.
- **Verify Publication:** Check the Nexus repository for the published package.

### Exercise 5: Create Maven Hosted Repository
**Objective:** Set up a Maven hosted repository in Nexus for Java projects.
- **Access Nexus Interface:** Navigate to the Nexus administrative UI.
- **Create Repository:** Set up a new Maven repository with appropriate configurations.
- **Save and Test:** Confirm the repository’s setup and test with a sample Maven project.

### Exercise 6: Create User for Team 2
**Objective:** Create a Nexus user for Team 2 with Maven repository access.
- **User Setup:** Open Nexus user management.
- **Define User:** Create a new user with necessary details.
- **Permissions:** Assign access to the Maven repository.
- **Save and Share:** Provide Team 2 with the new user credentials.

### Exercise 7: Build and Publish Jar File
**Objective:** Build a Java application and publish it to the Maven repository.
- **Java Project Preparation:** Work with a Java project.
- **Build Jar:** Use Maven to compile and package the application.
- **Publish to Nexus:** Upload the jar file to the Nexus Maven repository.
- **Verify in Nexus:** Ensure the jar file is correctly published in the repository.

### Exercise 8: Download from Nexus and Start Application
**Objective:** Download and run applications from both npm and Maven repositories.
- **User Setup:** Configure a user with access to both repositories.
- **Fetch Artifacts:** Download the latest NodeJS and Java artifacts.
- **Application Startup:** Run both applications on a server.
- **Check Execution:** Confirm the applications are running correctly.

### Exercise 9: Automate
**Objective:** Automate the process of fetching and starting the NodeJS application.
- **Script Writing:** Develop a script to automate the fetching of the latest NodeJS app version.
- **Include Execution Steps:** Ensure the script can untar and run the application.
- **Configure the .npmrc file:** Ensure .npmrc is created in the home directory of the user or the application directory.
- **Script Testing:** Validate the script's functionality.
- **Documentation:** Provide clear documentation for the script's operation and potential modifications.
[View the automation script](./scripts/nexus_app_script_v1.0.1.sh)

## Screenshots

### Screenshot 1: Nexus Server Setup
![artifact-repository-manager-with-nexus](./images/nexus_server_setup.png)

- **Description**: Screenshot of the Nexus server setup and initial configuration.

### Screenshot 2: npm Repository in Nexus
![artifact-repository-manager-with-nexus](./images/npm_repository_nexus.png)
- **Description**: Display the newly created npm hosted repository in Nexus.

### Screenshot 3: Nexus user for Team 1 with specific permissions
![artifact-repository-manager-with-nexus](./images/team_1_user_nexus.png)
- **Description**: View of the newly created user for Team 1 in Nexus.

### Screenshot 4: Published NodeJS Artifact
![artifact-repository-manager-with-nexus](./images/published_nodejs_artifact.png)
- **Description**: Screenshot showing the successful publication of the NodeJS tar package in the npm repository.

### Screenshot 5: Published Maven Artifact
![artifact-repository-manager-with-nexus](./images/published_maven_artifact.png)
- **Description**: Screenshot showing the successful publication of the Java jar file in the Maven repository.

### Screenshot 6: Nexus user for Team 2 with specific permissions
![artifact-repository-manager-with-nexus](./images/team_2_user_nexus.png)
- **Description**: View of the newly created user for Team 1 in Nexus.

### Screenshot 7: Build and Publish Jar File
![artifact-repository-manager-with-nexus](./images/java_project_preparation.png)
- **Description**: View of the Java Project that was used.

![artifact-repository-manager-with-nexus](./images/build_jar.png)
- **Description**: View of the use of Maven to compile and package the application.

![artifact-repository-manager-with-nexus](./images/publish_to_Nexus1.png)
- **Description**: View of the uploaded jar file to the Nexus Maven repository.

![artifact-repository-manager-with-nexus](./images/publish_to_Nexus2.png)
- **Description**: View of the uploaded jar file to the Nexus Maven repository.

![artifact-repository-manager-with-nexus](./images/Verify_in_Nexus.png)
- **Description**: View of the correctly published jar file in the repository.

### Screenshot 8: Download from Nexus and Start Application
![artifact-repository-manager-with-nexus](./images/User_Setup.png)
- **Description**: Screenshot of a user with access to the repository.

![artifact-repository-manager-with-nexus](./images/Fetch_Artifacts1.png)
- **Description**: Screenshot - latest NodeJS artifact.

![artifact-repository-manager-with-nexus](./images/Fetch_Artifacts2.png)
- **Description**: Screenshot - latest NodeJS artifact.

![artifact-repository-manager-with-nexus](./images/Application_Startup.png)
- **Description**: Screenshot - application on a droplet server.

![artifact-repository-manager-with-nexus](./images/Execution_check.png)
- **Description**: Screenshot - application running correctly.

### Screenshot 9: Automated Script Execution

![artifact-repository-manager-with-nexus](./images/Configure_.npmrc.png)
- **Description**: Screenshot - configure the .npmrc file

![artifact-repository-manager-with-nexus](./images/automated_script_execution.png)
- **Description**: Screenshot of the terminal running the automated script to fetch and start the NodeJS application.

## Usage
Follow the provided steps to set up Nexus, create repositories, manage users, and automate artifact deployment.

## Contributing
Contributions, issues, and feature requests are welcome. For major changes, please open an issue first.

## License
[artifact-repository-manager-with-nexus](./LICENSE)
