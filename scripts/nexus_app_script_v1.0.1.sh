#!/bin/bash

# Define variables
APP_NAME="bootcamp-node-project"
INSTALL_DIR="/opt/${APP_NAME}"
DEPLOY_DIR="/opt/${APP_NAME}/package"
ARTIFACT_JSON="artifact.json"
NEXUS_URL="http://<IP ADDRESS>:8081"
REPOSITORY="npm-hosted"
NX_USER="<USER>"
NX_PASSWORD="<PASSWORD>"

# Function to fetch and deploy the latest version
fetch_and_deploy_latest() {
    # Fetch artifact details and save to JSON file
    echo "Fetching artifact details..."
    if ! curl -s -u "${NX_USER}:${NX_PASSWORD}" -X GET "${NEXUS_URL}/service/rest/v1/components?repository=${REPOSITORY}&sort=version" | jq "." > "${ARTIFACT_JSON}"; then
        echo "Failed to fetch artifact details."
        return 1
    fi

    # Extract the download URL for the latest version
    artifactDownloadUrl=$(jq '[.items[] | select(.name=="'"${APP_NAME}"'") | .assets[].downloadUrl][-1]' "${ARTIFACT_JSON}" --raw-output)

    # Check if the URL is empty
    if [ -z "$artifactDownloadUrl" ]; then
        echo "No download URL found for ${APP_NAME}."
        return 1
    fi

    # Create a temporary directory
    TEMP_DIR=$(mktemp -d)

    # Download the latest version
    echo "Fetching the latest version of ${APP_NAME} from ${artifactDownloadUrl}..."
    if ! wget -q -O "${TEMP_DIR}/${APP_NAME}.tgz" --http-user="${NX_USER}" --http-password="${NX_PASSWORD}" "${artifactDownloadUrl}"; then
        echo "Failed to download the latest version of ${APP_NAME}."
        rm -rf "${TEMP_DIR}"
        return 1
    fi

    # Extract the tar file to the installation directory
    echo "Extracting files to ${INSTALL_DIR}..."
    mkdir -p "${INSTALL_DIR}"
    if ! tar -xzf "${TEMP_DIR}/${APP_NAME}.tgz" -C "${INSTALL_DIR}"; then
        echo "Failed to extract files."
        rm -rf "${TEMP_DIR}"
        return 1
    fi

    # Install dependencies (if needed)
    if [ -d "${DEPLOY_DIR}" ]; then
        cd "${DEPLOY_DIR}" || return 1
        echo "Installing dependencies for ${APP_NAME}..."
        if ! npm install; then
            echo "Failed to install dependencies."
            return 1
        fi

        # Start the application
        echo "Starting ${APP_NAME}..."
        if ! npm start; then
            echo "Failed to start ${APP_NAME}."
            return 1
        fi
    else
        echo "Deployment directory ${DEPLOY_DIR} does not exist."
        return 1
    fi

    # Clean up
    rm -rf "${TEMP_DIR}"
    return 0
}

# Main script execution
if [ -d "${INSTALL_DIR}" ]; then
    # If the installation directory exists, stop the existing app
    echo "Stopping existing ${APP_NAME}..."
    pkill -f "${APP_NAME}"

    # Fetch and deploy the latest version
    if fetch_and_deploy_latest; then
        echo "Deployment completed successfully."
    else
        echo "Deployment failed."
    fi
else
    # If the installation directory doesn't exist, create it
    mkdir -p "${INSTALL_DIR}"

    # Fetch and deploy the latest version
    if fetch_and_deploy_latest; then
        echo "Deployment completed successfully."
    else
        echo "Deployment failed."
    fi
fi